# Bank accounting

Bank accounting is an application that simulates bank accounting transfers and balances.

## Prerequisites

Use [docker](https://docs.docker.com/) and [docker-compose](https://docs.docker.com/compose/).

## Installing

To see step by step application start up see `Dockerfile` and `docker-compose.yml`.

`docker-compose up`

## Running tests

The tests are made using rspec:

`docker-compose run --rm bundle exec rspec`

See tests files on spec folder

## CI/CD

See gitlab-ci.yml to check more details

## TODOS

- Add a ci step to run tests

## Author

- Claudio Melo - [LinkedIn](https://www.linkedin.com/in/claudio-melo/)