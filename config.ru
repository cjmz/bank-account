# frozen_string_literal: true

require File.expand_path('config/environment', __dir__)

if ENV['RACK_ENV'] == 'development'
  Bundler.require(:default, :development)
else
  Bundler.require(:default)
end

use Rack::CommonLogger if ENV['RACK_ENV'] == 'production'
use Rack::ConditionalGet
use Rack::ETag
use Rack::Cors do
  allow do
    origins '*'
    resource '/api/*',
             headers: :any,
             methods: %i[get post put delete options]
  end
end

run API::Base
