class CreateTransfers < ActiveRecord::Migration[6.0]
  def change
    create_table :transfers do |t|
      t.string :account_id, required: true
      t.numeric :amount, required: true

      t.timestamps
    end
  end
end
