FROM ruby:2.7.0


ENV LANG=C.UTF-8 \
    LANGUAGE=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    TZ=America/Sao_Paulo

RUN apt-get update

WORKDIR /api

EXPOSE 8080