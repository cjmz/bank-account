# frozen_string_literal: true

module API
  module V1
    # Balance routes controller
    class Balance < Sinatra::Base
      helpers Authenticate

      get '/balance' do
        protected!
        account_id = params[:account_id]

        ::Transfer.balance(account_id).to_s
      end
    end
  end
end
