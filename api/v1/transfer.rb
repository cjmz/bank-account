# frozen_string_literal: true

module API
  module V1
    # Transfer routes controller
    class Transfer < Sinatra::Base
      helpers Sinatra::Param

      get '/transfer' do
        ::Transfer.connection

        param :source_account_id, String, required: true
        param :destination_account_id, String, required: true
        param :amount, Float, required: true

        # VALIDAR DADOS (SOURCE_ACCOUNT_ID, DESTINATION_ACCOUNT_ID E AMOUNT)
        source_account_id = params[:source_account_id]
        destination_account_id = params[:destination_account_id]
        amount = params[:amount]

        # CANCELA TRANSFERENCIA SE CONTA ORIGEM NAO TIVER SALDO SUFICIENTE
        halt 401 unless founds?(source_account_id, amount)

        # COMPUTA DEBITO CONTA ORIGEM
        ::Transfer.create(account_id: source_account_id, amount: -amount.to_f)

        # COMPUTA CREDITO DESTINO
        ::Transfer.create(account_id: destination_account_id,
                          amount: amount.to_f)

        { 'success': 'success' }.to_json
      end

      private

      def founds?(destination_account_id, amount)
        APP::Support::AccountValidation.have_founds?(destination_account_id,
                                                     amount)
      end
    end
  end
end
