# frozen_string_literal: true

require_relative 'v1/balance'
require_relative 'v1/transfer'

module API
  # Base routes
  class Base < Sinatra::Base
    use API::V1::Balance
    use API::V1::Transfer
  end
end
