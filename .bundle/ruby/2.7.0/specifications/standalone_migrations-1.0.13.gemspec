# -*- encoding: utf-8 -*-
# stub: standalone_migrations 1.0.13 ruby lib

Gem::Specification.new do |s|
  s.name = "standalone_migrations".freeze
  s.version = "1.0.13"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Todd Huss".freeze, "Michael Grosser".freeze]
  s.date = "2012-09-26"
  s.email = "thuss@gabrito.com".freeze
  s.extra_rdoc_files = ["README.markdown".freeze]
  s.files = ["README.markdown".freeze]
  s.homepage = "http://github.com/thuss/standalone-migrations".freeze
  s.rubygems_version = "3.1.2".freeze
  s.summary = "A thin wrapper to use Rails Migrations in non Rails projects".freeze

  s.installed_by_version = "3.1.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<rake>.freeze, [">= 0"])
    s.add_runtime_dependency(%q<activerecord>.freeze, [">= 3"])
  else
    s.add_dependency(%q<rake>.freeze, [">= 0"])
    s.add_dependency(%q<activerecord>.freeze, [">= 3"])
  end
end
