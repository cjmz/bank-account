# frozen_string_literal: true

require File.expand_path('environment', __dir__)

require 'require_all'
require 'sinatra'
require 'active_record'
require 'sinatra/param'

require_rel './../app/**/*.rb'
require_rel './../api/**/*.rb'
