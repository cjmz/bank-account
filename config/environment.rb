# frozen_string_literal: true

ENV['RACK_ENV'] ||= 'production'

# ActiveRecord::Base.establish_connection(ENV['RACK_ENV'].to_sym)

require File.expand_path('application', __dir__)
