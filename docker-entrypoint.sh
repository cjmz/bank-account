#!/bin/bash

set -ex

cd /api

unset BUNDLE_APP_CONFIG
bundle install
bundle config set path '/vendor/bundle'

exec "$@"
