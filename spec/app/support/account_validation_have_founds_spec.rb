# frozen_string_literal: true

require 'spec_helper'

describe APP::Support::AccountValidation, '.have_founds?' do
  subject(:account_exists?) { described_class.have_founds?(account_id, amount) }

  let(:account_id) { 'oi' }
  let(:amount) { 35 }

  context 'when do not have found' do
    before do
      allow(::Transfer).to receive(:balance).with(account_id).and_return(15)
    end

    it { is_expected.to eq(false) }
  end

  context 'when have founds' do
    before do
      allow(::Transfer).to receive(:balance).with(account_id).and_return(65)
    end

    it { is_expected.to eq(true) }
  end
end
