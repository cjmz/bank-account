# frozen_string_literal: true

require 'spec_helper'

describe APP::Support::AccountValidation, '.account_exists?' do
  subject(:account_exists?) { described_class.account_exists?(account_id) }

  let(:account_id) { 'oi' }

  context 'when do not exists an account' do
    before do
      allow(::Transfer).to receive(:account_exists?).with(account_id)
                                                    .and_return(nil)
    end

    it { is_expected.to eq(nil) }
  end

  context 'when exists an account' do
    before do
      allow(::Transfer).to receive(:account_exists?).with(account_id)
                                                    .and_return(true)
    end

    it { is_expected.to eq(true) }
  end
end
