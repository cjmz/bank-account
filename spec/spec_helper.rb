# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require File.expand_path('../config/environment', __dir__)

ActiveRecord::Migration.maintain_test_schema!
ActiveRecord::Base.establish_connection(adapter: 'postgresql',
                                        host: 'db',
                                        user: 'postgres',
                                        password: 'example',
                                        database: 'test_bank_account')

require 'require_all'
require 'sinatra'
require 'active_record'
require 'rack/test'
require 'rspec'

module RSpecMixin
  include Rack::Test::Methods

  def app
    API::V1::Transfer
  end
end

require_rel './../app/**/*.rb'
require_rel './../api/**/*.rb'

RSpec.configure do |config|
  config.filter_run_excluding perf: true
  config.include RSpecMixin
end
