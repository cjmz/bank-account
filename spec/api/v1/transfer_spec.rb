# frozen_string_literal: true

require 'spec_helper'

describe API::V1::Transfer, '.transfer' do
  subject { last_response.status }

  describe 'when have founds' do
    before do
      allow(APP::Support::AccountValidation).to receive(:have_founds?)
        .and_return(true)
      get '/transfer?source_account_id=oi&destination_account_id=v&amount=32.3'
    end

    it { is_expected.to eq(200) }
  end

  describe 'when do not have founds' do
    before do
      get '/transfer?source_account_id=eee&destination_account_id=v&amount=32.3'
    end

    it { is_expected.to eq(401) }
  end
end
