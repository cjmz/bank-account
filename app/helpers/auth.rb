# frozen_string_literal: true

require 'sinatra'

# Authenticate helper
module Authenticate
  def protected!
    return if authorized?

    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, "Not Authorized\n"
  end

  def authorized?
    @auth ||= request.env['HTTP_X_AUTH']
    !@auth.empty? && @auth == '123456'
  end
end
