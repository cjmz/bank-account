# frozen_string_literal: true

# Transfer model
class Transfer < ActiveRecord::Base
  def self.account_exists?(account_id)
    where(account_id: account_id).first
  end

  def self.balance(account_id)
    ::Transfer.where(account_id: account_id).sum(:amount).to_i
  end
end
