# frozen_string_literal: true

module APP
  module Support
    # Account validations and support
    class AccountValidation
      class << self
        def founds?(account_id, amount)
          balance = ::Transfer.balance(account_id)

          return true if balance >= amount.to_i

          false
        end
      end
    end
  end
end
